package by.lpe.rentapartment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RentAnApartmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(RentAnApartmentApplication.class, args);
	}
}
